(function () {
  'use strict';

  /**
   * Search for libraries.
   *
   * Triggers when select libraries text field is changed by user.
   */
  document.body.addEventListener('input', function (event) {
    if (!event.target.id.startsWith('settings.field_libraries_')) {
      return;
    }

    var librariesData = drupalSettings.sitestudio_extras.libraries;

    var autocompleteList = document.createElement('ul');
    autocompleteList.classList.add('sitestudio-extras-autocomplete-list');
    document.body.appendChild(autocompleteList);

    var inputField = event.target;

    /**
     * Returns suggestions based on search keyword.
     */
    function handleAutocomplete(term) {
      // Split search terms into list.
      var keywords = term.split(' ');
      var suggestions = [];
      Object.entries(librariesData).forEach(([key, element]) => {
        var label = element.toLowerCase();

        // Add exact matches.
        if (label.indexOf(term.toLowerCase()) >= 0) {
          suggestions.push(element);
        } else {
          var matchCount = keywords.reduce((count, keyword) => {
            return count + (label.indexOf(keyword.toLowerCase()) >= 0 ? 1 : 0);
          }, 0);

          if (matchCount === keywords.length) {
            suggestions.push(element);
          }
        }
      });
      return suggestions;
    }

    // Handle input changes.
    inputField.addEventListener('input', function () {
      var term = inputField.value.trim();
      var suggestions = handleAutocomplete(term);

      // Clear old suggestions.
      autocompleteList.innerHTML = '';

      if (!suggestions.length) {
        var noResult = document.createElement('li');
        noResult.textContent = 'No Results';
        noResult.classList.add('no-result');
        autocompleteList.appendChild(noResult);
      } else {
        suggestions.forEach((suggestion) => {
          var item = document.createElement('li');
          item.textContent = suggestion;
          autocompleteList.appendChild(item);

          item.addEventListener('mousedown', function () {
            inputField.value = suggestion;
            autocompleteList.innerHTML = '';
          });
        });
      }

      // Style and position the list dynamically.
      var rect = inputField.getBoundingClientRect();
      autocompleteList.style.position = 'fixed';
      autocompleteList.style.top = `${rect.bottom}px`;
      autocompleteList.style.left = `${rect.left}px`;
      autocompleteList.style.width = `${rect.width}px`;
      autocompleteList.style.zIndex = getComputedStyle(
        document.querySelector('.ssa-app .ssa-modal-backdrop')
      ).zIndex;
    });

    // Close autocomplete when clicking outside.
    function handleOutsideClick(event) {
      if (!autocompleteList.contains(event.target) && event.target !== inputField) {
        autocompleteList.innerHTML = '';
      }
    }

    // Attach outside click listener.
    document.addEventListener('click', handleOutsideClick);

    // Cleanup when input is no longer needed
    inputField.addEventListener('blur', function () {
      document.removeEventListener('click', handleOutsideClick);
      autocompleteList.remove();
    });
  });

})();
